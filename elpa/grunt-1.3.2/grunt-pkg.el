;;; -*- no-byte-compile: t -*-
(define-package "grunt" "1.3.2" "Some glue to stick Emacs and Gruntfiles together" '((dash "2.9.0") (ansi-color "3.4.2")) :url "https://github.com/gempesaw/grunt.el" :keywords '("convenience" "grunt"))

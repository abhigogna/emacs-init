;; Setup C-c r to run racket mode
;; (add-hook 'racket-mode-hook
;;           (lambda ()
;;             (define-key racket-mode-map (kbd "C-c r") 'racket-run)))


;;Unicode input method enabled
(add-hook 'racket-mode-hook      #'racket-unicode-input-method-enable)
(add-hook 'racket-repl-mode-hook #'racket-unicode-input-method-enable)

;; Enable paredit for Clojure
(add-hook 'racket-mode-hook 'enable-paredit-mode)
(add-hook 'racket-repl-mode-hook 'enable-paredit-mode)


;;Enable Rainbow delimeters
;; (add-hook 'racket-mode-hook 'rainbow-delimiters)
;; (add-hook 'racket-repl-mode-hook 'rainbow-delimeters)
(add-hook 'racket-mode-hook
            (lambda ()
              (rainbow-delimiters-mode -1)
              (rainbow-delimiters-mode)))

(add-hook 'racket-repl-mode-hook
            (lambda ()
              (rainbow-delimiters-mode -1)
              (rainbow-delimiters-mode)))

;;Autocomplete with tab
(setq tab-always-indent 'complete)

;;Docs
(add-hook 'racket-mode-hook 'eldoc-mode)

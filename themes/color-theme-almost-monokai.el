;;; Almost Monokai: color-theme-almost-monokai.el
;;; A beautiful, fruity and calm emacs color theme.

;; Author: Prateek Saxena <prtksxna@gmail.com>
;; Author: Pratul Kalia   <pratul@pratul.in>
;;
;; Based on the Monokai TextMate theme
;; Author: Wimer Hazenberg <http://www.monokai.nl>

;; Depends: color-theme

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

; Color theme support is required.
(require 'color-theme)

; plist-to-alist removed in emacs 24, re-add it
(if (version<= emacs-version "24")
  () ; do nothing for >= version 24
    (defun plist-to-alist (the-plist)
      (defun get-tuple-from-plist (the-plist)
        (when the-plist
          (cons (car the-plist) (cadr the-plist))))
      (let ((alist '()))
        (while the-plist
          (add-to-list 'alist (get-tuple-from-plist the-plist))
          (setq the-plist (cddr the-plist)))
        alist))
  )

; Code start.
(defun color-theme-almost-monokai ()
  (interactive)
  (color-theme-install
   '(color-theme-almost-monokai
     ((background-color . "#272821")
      (foreground-color . "#F8F8F2")
      (cursor-color . "#EBD739"))
     (default ((t (nil))))
     ;;(modeline ((t (:background "white" :foreground "black" :box (:line-width 1 :style released-button)))))
     (font-lock-builtin-face ((t (:foreground "#A6E22A"))))
     (font-lock-comment-face ((t (:italic t :foreground "#75715D"))))
     (font-lock-constant-face ((t (:foreground "#A6E22A"))))
     (font-lock-doc-string-face ((t (:foreground "#65B042"))))
     (font-lock-string-face ((t (:foreground "#DFD874"))))
     (font-lock-function-name-face ((t (:foreground "#F1266F" :italic t))))
     (font-lock-keyword-face ((t (:foreground "#66D9EF"))))
     (font-lock-type-face ((t (:underline nil :foreground "#89BDFF"))))
     (font-lock-variable-name-face ((t (:foreground "#A6E22A"))))
     (font-lock-warning-face ((t (:bold t :foreground "#FD5FF1"))))
     (highlight-80+ ((t (:background "#D62E00"))))
     (hl-line ((t (:background "#1A1A1A"))))
     (region ((t (:background "#6DC5F1"))))
     (ido-subdir ((t (:foreground "#F1266F"))))
     (fringe ((t (:background "#1E2126" ))))
     (mode-line ((t (:foreground "#66D9EF" :background "#272821"))))
     
     ;; Company mode
     (company-tooltip ((t (:foreground "#66D9EF" :background "#272821"))))
     (company-tooltip-annotation ((t (:foreground "#eacc8c"))))
     (company-tooltip-selection ((t (:background "#464a4d"))))
     (company-tooltip-mouse ((t (:background "#464a4d"))))
     (company-tooltip-common ((t (:foreground "#909fab"))))
     (company-scrollbar-fg ((t (:background "#464a4d"))))
     (company-scrollbar-bg ((t (:background "#242a34"))))
     (company-preview ((t (:foreground "#bdc3ce" :background "#242a34"))))
     (company-preview-common ((t (:foreground "#909fab"))))

     ;;Rainbow Delimiters
     (rainbow-delimiters-depth-1-face ((t (:foreground "SteelBlue1"))))
     (rainbow-delimiters-depth-2-face ((t (:foreground "deep pink"))))
     (rainbow-delimiters-depth-3-face ((t (:foreground "chartreuse"))))
     (rainbow-delimiters-depth-4-face ((t (:foreground "deep sky blue"))))
     (rainbow-delimiters-depth-5-face ((t (:foreground "yellow"))))
     (rainbow-delimiters-depth-6-face ((t (:foreground "orchid"))))
     (rainbow-delimiters-depth-7-face ((t (:foreground "spring green"))))
     (rainbow-delimiters-depth-8-face ((t (:foreground "sienna1"))))
    )))

(provide 'color-theme-almost-monokai)
;---------------
; Code end.
